Los requerimientos de instalación son los que indica Laravel:

- PHP >= 7.0.0
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

Una vez clonado el proyecto, se deben instalar las dependencias de Composer (es necesario instalar "composer": https://getcomposer.org/) y luego, en el path del proyecto (en mi caso /var/www/html/bondacom): 
#composer install
Si no se tiene creada la carpeta storage, es necesario crearla con subcarpetas app, framework y logs. Todo el contenido de storage y el boostrap/cache deben tener permisos de escritura (en Linux 777). Recomendable aplicar: 
#sudo chmod -R 777 storage boostrap/cache

Puede verse que se requiere mysql y luego crear una base de datos (en mi caso, la llamé bondacom pero es indistinto).

En el path principal del proyecto, debe crearse un archivo que tome variables de ambiente llamado .env (ver el .env.example que se encuentra en el path principal del proyecto). Dentro de ese .env se debe configurar los datos de la base de datos recientemente creada.

Una vez realizado esto, pararse en el path del proyecto para correr las migraciones:
#php artisan migrate

Para poder utilizar el proyecto, en mi caso utilicé Apache2 en Linux, y por eso tuve que configurar lo siguiente:

En /etc/hosts agregué al final de todo la siguiente línea:
#127.0.0.1  bondacom-com

En /etc/apache2/sites-enabled, creé un archivo bondacom.conf, el cual tiene lo siguiente:

```
<VirtualHost *:80>
  ServerName bondacom-com
  DocumentRoot "/var/www/html/bondacom/public"
  <Directory "/var/www/html/bondacom/public">
    AllowOverride all
  </Directory>
</VirtualHost>
```
Luego, aplicar en la terminal: 
#sudo a2enmod rewrite
De esta manera se podrá utilizar bien el ruteo del proyecto que propone Laravel por defecto. 

------------------------------------------------------------------------
API
Todos los métodos son GET y llevan como campo obligatorio "api_credential"

- bondacom-com/api/locations/add
		Campo obligatorio adicional: location (ejemplo Argentina,Buenos Aires,Quilmes)
		Permisos: write o admin
		Funcionamiento: Crea todas las localizaciones que se encuentran delimitadas por ','. Si ya estaban creadas, simplemente lo omite. En ambos casos devuelve el id de la localización más chica, junto con toda la localización. Para el caso de Argentina,Buenos Aires,Quilmes se devuelve el id de Quilmes junto con argentina,buenos aires,quilmes (se guarda en lowercase cada localización).

- bondacom-com/api/locations/delete
		Campo obligatorio adicional: location (ejemplo Argentina,Buenos Aires,Quilmes)
		Permisos: delete o admin
		Funcionamiento: Borra la localización "hoja" del 'location' que se recibe por parámetro. Ejemplo, si se recibe 'Argentina,Buenos Aires,Quilmes', entonces se borra Quilmes.

- bondacom-com/api/locations/search:
		Campo obligatorio adicional: location (ejemplo Quilmes)
		Permisos: read o admin
		Funcionamiento: Devuelve la localización completa para todas las localizaciones que se llaman como el parámetro 'location' (puede ocurrir). El formato del response es un array tipo json que contiene todas estos matches. Para este ejemplo, asumiendo un id igual a 3 Quilmes, el response será así: {"#3 - Argentina,Buenos Aires,Quilmes"}

- bondacom-com/api/locations/childs
		Campo obligatorio adicional: location_id (ejemplo )
		Permisos: read o admin
		Funcionamiento: Devuelve los hijos de una localización en formato de árbol plano json. Si no tiene hijos, lo indica. Si una localización tiene hijos, se incluye a este como nodo principal del árbol

-----------------------------------------------------------------------
CREDENCIALES DE LA API:
Son 4. Una con permisos de escritura (bondacom_write en md5), otra de lectura (bondacom_read), otra de borrado (bondacom_delete), y otra de admin (bondacom_admin) que tiene habilitados todos los permisos anteriores.
 
		write: 7f2e519a678376b94ae485b5f5a45280
		read => 5c3bd2b3287c944ad566763c5366b54d
		delete => 9704cde458d35c98aae18879c708006e
		admin => 29593c879b05d83296a9a789bce0ec2f
