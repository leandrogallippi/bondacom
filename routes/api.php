<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/locations/add', 'Api\LocationsController@add')->middleware(App\Http\Middleware\AddWritePermissions::class);
Route::get('/locations/delete', 'Api\LocationsController@delete')->middleware(App\Http\Middleware\AddDeletePermissions::class);
Route::get('/locations/search', 'Api\LocationsController@search')->middleware(App\Http\Middleware\AddReadPermissions::class);
Route::get('/locations/childs', 'Api\LocationsController@childs')->middleware(App\Http\Middleware\AddReadPermissions::class);
