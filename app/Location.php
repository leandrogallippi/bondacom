<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    
    /**
     * Get the sub-locations for the location.
     */
    public function subLocations()
    {
        return $this->hasMany('App\Location', 'parent_id', 'id');
    }

    /**
     * Get the parent location that owns the location.
     */
    public function parentLocation()
    {
        return $this->belongsTo('App\Location', 'parent_id');
    }

    public function delete() {
        $this->subLocations()->delete();
        parent::delete();
    }

    /**
     * @return string
     */
    public function getFullName() {
        $parentLocation = $this;
        $fullName = $this->name;
        
        while($parentLocation = $parentLocation->parentLocation) {
            $fullName = $parentLocation->name . ',' . $fullName;
        }
        return $fullName;
    }
}
