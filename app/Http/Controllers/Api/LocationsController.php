<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;

class LocationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('api.security');
    }


    /**
     * @param string $string
     * @return nil or array
     */
    private function parseLocationNames($string) {
        $stringExploded = explode(',', $string);

        if (count($stringExploded) >= 2) {
            $locationNames = [];

            foreach ($stringExploded as $locationName) {
                $locationNames[] = strtolower(trim($locationName));
            }

            return $locationNames;
        }
    }



    /**
     * Add location.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if (!$request->has('location')) {
            return response()->json(["Falta el atributo 'location'"]);
        }
        if (($locationNames = $this->parseLocationNames($request->location)) === null) {
            return response()->json(["El atributo 'location' no tiene el formato esperado. Debe ser delimitado por comas, ejemplo: Argentina,Buenos Aires,Quilmes"]);
        }

        $parentId = null;

        foreach ($locationNames as $locationName) {
            $location = Location::where('name', $locationName)
                            ->where('parent_id', $parentId)
                            ->first();
            if (!$location) {
                $location = new Location();
                $location->name = $locationName;
                $location->parent_id = $parentId;
                $location->save();
            }
            $parentId = $location->id;
        }

        return response()->json(['#' . $location->id . ': ' . $location->getFullName()]);

    }

    /**
     * Delete location.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if (!$request->has('location')) {
            return response()->json(["Falta el atributo 'location'"]);
        }
        if (($locationNames = $this->parseLocationNames($request->location)) === null) {
            return response()->json(["El atributo 'location' no tiene el formato esperado. Debe ser delimitado por comas, ejemplo: Argentina,Buenos Aires,Quilmes"]);
        }

        $parentId = null;
        foreach ($locationNames as $locationName) {
            $location = Location::where('name', $locationName)
                            ->where('parent_id', $parentId)
                            ->first();
            if (!$location) {
                return response()->json(["Localidad inexistente"]);
            }
            $parentId = $location->id;
        }

        if ($location) {
            $location->delete();
        }
        return response()->json(["Localización borrada de manera exitosa"]);

    }

    /**
     * Search a specific location.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->has('location')) {
            return response()->json(["Error"]);
        }

        $locations = Location::where('name', $request->location)->get();

        $results = [];

        foreach ($locations as $location) {

            $results[] = '#' . $location->id . ': ' . $location->getFullName();    
        }

        return response()->json($results);

    }

    /**
     * Get childs from a specific location.
     *
     * @return \Illuminate\Http\Response
     */
    public function childs(Request $request)
    {
        if (!$request->has('location_id')) {
            return response()->json(["Falta el atributo 'location_id'"]);
        }

        $location = Location::where('id', $request->location_id)->first();

        if (!$location) {
            return response()->json(["Localización Inválida"]);
        }

        $childs = $this->getChilds($location);

        if (!is_array($childs)) {
            return response()->json(["La localización '$location->name' no tiene hijos"]);
        }

        return response()->json($childs);

    }


    /**
     * @param App\Location $location
     * @return array
     */
    private function getChilds($location) {
        $subLocations = $location->subLocations;

        if (count($subLocations) == 0) {
            return $location->name;
        }
        $result = [$location->name => []];
        foreach ($subLocations as $subLocation) {
            $result[$location->name][] = $this->getChilds($subLocation);
        }
        return $result;
    }
}
