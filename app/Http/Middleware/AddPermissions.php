<?php

namespace App\Http\Middleware;

use Closure;

class AddPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected function validRequest($request, Closure $next, $apiCredential)
    {
        if (!$request->has('api_credential') || ($request->api_credential != md5('bondacom_admin') && $request->api_credential != md5($apiCredential))) {

            return response()->json(["Acceso inválido"]);
        }
        return $next($request);
    }
}
